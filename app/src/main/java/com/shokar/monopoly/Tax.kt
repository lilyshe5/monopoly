package com.shokar.monopoly

class Tax(override val name: String = "Tax",
          override val type: String = "Tax"): Card(name, type, buyCost = 99999) {

    override fun action() {
    }

    override fun rentCost(): Int {
        return 200
    }

    override fun freeCard() {
    }
}