package com.shokar.monopoly

class Player(val name: String) {

    var money = 1000
    var isInPrison = false
    var currentPosition = 0
    private var prevStep = 0

    val card = mutableListOf<Card>()

    fun addCard(card: Card){
        this.card.add(card)
    }

    fun move(count: Int){
        currentPosition += count
        if (currentPosition > 39)
            money += 200

        prevStep = currentPosition
    }
}