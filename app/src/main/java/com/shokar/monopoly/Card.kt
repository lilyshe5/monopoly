package com.shokar.monopoly

abstract class Card(open val name: String,
                    open val type: String,
                    open val buyCost: Int) {

    abstract fun action()
    abstract fun rentCost(): Int
    abstract fun freeCard()
}