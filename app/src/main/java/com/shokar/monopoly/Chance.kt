package com.shokar.monopoly

class Chance(override val name: String = "Chance",
             override val type: String = "Chance",
             override val buyCost: Int = 99999): Card(name, type, buyCost) {
    override fun action() {

    }

    override fun rentCost(): Int {
        return 0
    }

    override fun freeCard() {

    }
}