package com.shokar.monopoly

class Prison(override val name: String = "Prison",
             override val type: String = "Prison"): Card(name, type, 99999) {

    override fun action() {
        
    }

    override fun rentCost(): Int {
        return 0
    }

    override fun freeCard() {

    }
}