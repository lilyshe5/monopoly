package com.shokar.monopoly

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_game.*

class GameActivity : AppCompatActivity() {

    val card = mutableListOf<Card>()
    val player = mutableListOf<Player>()
    val playerMargin = mutableListOf(Pair(0,0), Pair(60, 0), Pair(0, 60), Pair(60, 60))

    var playerCount = 2
    var isDouble = false
    var isDoubleCount = 0
    val FieldStep = 120

    var currentPlayer = 0
    var currentField = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        playerCount = intent.getIntExtra("count", 2)

        InitCards()
        InitPlayers(playerCount)
        ShowMoney()
        currentPlayerTextView.text = "Current Player: ${player[currentPlayer].name}"

        CreatePlayers()

        StartGame()

        throwButton.setOnClickListener {
            val step = ThrowCube()
            if (!player[currentPlayer].isInPrison || isDouble) {
                player[currentPlayer].move(step)

                MovePlayer(step, player[currentPlayer].currentPosition - step)

                player[currentPlayer].currentPosition %= 40
                currentField = player[currentPlayer].currentPosition
                action(player[currentPlayer].currentPosition)
            } else {
                passButton.visibility = View.VISIBLE
            }
        }

        buyButton.setOnClickListener {
            var i = -1

            if (card[currentField].type == "Street")
                i = ifIsFull()

            if (i != -1){
                val a = player[currentPlayer].card[i] as Street
                if (a.houseCount == 5){
                    actionView.text = "Max house count"
                } else
                if (player[currentPlayer].money - a.houseRent[a.houseCount] >= 0){
                    player[currentPlayer].money -= a.houseRent[a.houseCount]
                    ++a.houseCount
                    ShowMoney()
                }

            }else if (player[currentPlayer].money - card[currentField].buyCost >= 0 ) {

                if (card[currentField] in player[currentPlayer].card){
                    actionView.text = "It's your already"
                    return@setOnClickListener
                }

                player[currentPlayer].addCard(card[currentField])
                when (card[currentField].type){
                    "Street" -> {
                        val a = player[currentPlayer].card.last() as Street
                        FindFullStack(a.color)
                    }
                    "Railway" -> {
                        val a = player[currentPlayer].card.last() as Railway
                        ++a.railwayCount
                    }
                    "Utilities" -> {
                        val a = player[currentPlayer].card.last() as Utilities
                        ++a.count
                    }
                }

                player[currentPlayer].money -= card[currentField].buyCost
                ShowMoney()
            } else
                actionView.text = "Not enough money!"
        }

        passButton.setOnClickListener {
            if(isDouble){
                if (isDoubleCount == 3) {
                    isDouble = false
                    isDoubleCount = 0
                    player[currentPlayer].isInPrison = true
                }
                else
                    StartGame()
            }
            else {
                currentPlayer = (currentPlayer + 1) % playerCount
                currentPlayerTextView.text = "Current Player: $currentPlayer"
                StartGame()
            }
        }


    }

    private fun StartGame(){
        buyButton.visibility = View.GONE
        throwButton.visibility = View.VISIBLE
        passButton.visibility = View.GONE
    }

    private fun ThrowCube(): Int{
        val a = (Math.random() * 5 + 1).toInt()
        val b = (Math.random() * 5 + 1).toInt()

        actionView.text = "First: $a\nSecond: $b"

        if (a == b) {
            isDouble = true
            ++isDoubleCount
        } else{
            isDouble = false
            isDoubleCount = 0
        }

        return a + b
    }

    private fun action(position: Int){
        passButton.visibility = View.VISIBLE
        throwButton.visibility = View.GONE

        when (card[position].type){
            "Street" -> {
                val a = card[position] as Street
                var own = ""

                if (a.owner == null){
                    buyButton.visibility = View.VISIBLE
                } else if (a.owner != player[currentPlayer]){
                    PayToPlayer(FindOwner(a.owner), a.rentCost())
                    actionView.text = "${player[currentPlayer].name} pay ${a.owner!!.name} ${a.rentCost()} money!"
                    own = a.owner!!.name
                }
                logoView.text = "CurrentPosition: ${currentField + 1}\nName: ${a.name}\nOwner: $own\nHouseCount: ${a.houseCount}\nRentCost: ${a.rentCost()}"
            }
            "Railway" -> {
                val a = card[position] as Railway
                var own = ""

                if (a.owner == null) {
                    buyButton.visibility = View.VISIBLE
                } else if (a.owner != player[currentPlayer]) {
                    PayToPlayer(FindOwner(a.owner), a.rentCost())
                    actionView.text = "${player[currentPlayer].name} pay ${a.owner!!.name} ${a.rentCost()} money!"
                    own = a.owner!!.name
                }
                logoView.text = "CurrentPosition: ${currentField + 1}\nName: ${a.name}\nOwner: $own\nRailwayCount: ${a.railwayCount}\nRentCost: ${a.rentCost()}"
            }
            "Tax" -> {
                val a = card[position] as Tax
                PayToBank(a.rentCost())
                actionView.text = "${player[currentPlayer].name} pay ${a.rentCost()} money to bank!"
            }
            "Utilities" -> {
                val a = card[position] as Utilities
                var own = ""

                if (a.owner == null){
                    buyButton.visibility = View.VISIBLE
                } else if (a.owner != player[currentPlayer]){
                    PayToPlayer(FindOwner(a.owner), a.rentCost())
                    actionView.text = "${player[currentPlayer].name} pay ${a.owner!!.name} ${a.rentCost()} money!"
                    own = a.owner!!.name
                }
                logoView.text = "CurrentPosition: ${currentField + 1}\nName: ${a.name}\nOwner: $own\nUtilitiesCount: ${a.count}\nRentCost: ${a.rentCost()}"
            }
            "Parking" -> {
                actionView.text = "Free parking!"
            }
            "Prison" -> {
                val a = card[position] as Prison

                actionView.text = "${player[currentPlayer].name} go to the prison!"

                val cs = gameField.getChildAt(12 + currentPlayer).layoutParams as ConstraintLayout.LayoutParams
                cs.marginEnd = 1200
                cs.bottomMargin = 0
                gameField.getChildAt(12 + currentPlayer).layoutParams = cs

                player[currentPlayer].isInPrison = true
                player[currentPlayer].currentPosition = 10
            }
            "Chance" -> {
                val a = card[position] as Chance
                val b = (Math.random() * 200).toInt()

                actionView.text = "Chance! ${player[currentPlayer].name} get $b money!"

                player[currentPlayer].money += b
            }
        }
        ShowMoney()
    }

    private fun PayToBank(rent: Int){
        if (player[currentPlayer].money - rent > 0) {
            player[currentPlayer].money -= rent
        }
        else{
            //sell houses if not enough money
            if (SellHouse(rent)){
                player[currentPlayer].money -= rent
            }
            else{
                //sell streets if still not enough money
                if (SellStreet(rent)){
                    player[currentPlayer].money -= rent
                } else {
                    //can't pay rent
                    GameOver()
                }
            }
        }
        ShowMoney()
    }

    private fun PayToPlayer(owner: Int, rent: Int){
        //pay rent other player
        if (player[currentPlayer].money - rent > 0) {
            player[currentPlayer].money -= rent
            player[owner].money += rent

        }
        else{
            //sell houses if not enough money
            if (SellHouse(rent)){
                player[currentPlayer].money -= rent
                player[owner].money += rent
            }
            else{
                //sell streets if still not enough money
                if (SellStreet(rent)){
                    player[currentPlayer].money -= rent
                    player[owner].money += rent
                } else {
                    //can't pay rent
                    player[owner].money += player[currentPlayer].money
                    GameOver()
                }
            }
        }
        ShowMoney()
    }

    private fun FindOwner(owner: Player?): Int{
        for (i in 0 until player.count()){
            if (owner == player[i])
                return i
        }
        return -1
    }

    private fun SellHouse(rent: Int): Boolean{
        for (i in 0 until player[currentPlayer].card.count()){
            val card = player[currentPlayer].card[i] as Street
            while (card.houseCount > 0 && player[currentPlayer].money - rent < 0){
                --card.houseCount
                player[currentPlayer].money += card.houseCost / 2
            }
            if (player[currentPlayer].money - rent >= 0)
                return true
        }
        return false
    }

    private fun SellStreet(rent: Int): Boolean{
        for (i in 0 until player[currentPlayer].card.count()){
            var a: Card
            when (player[currentPlayer].card[i].type){
                "Street" ->{
                    a = player[currentPlayer].card[i] as Street
                    if (!a.isPledge && player[currentPlayer].money - rent < 0){
                        a.isPledge = true
                        player[currentPlayer].money += a.sellCost

                        for (j in 0 until player[currentPlayer].card.count()){
                            if (player[currentPlayer].card[j].type == "Street") {
                                val b = player[currentPlayer].card[j] as Street
                                if (b.color == a.color) {
                                    b.isFullPack = false
                                }
                            }
                        }

                    }
                }
                "Railway" ->{
                    a = player[currentPlayer].card[i] as Railway
                    if (!a.isPledge && player[currentPlayer].money - rent < 0){
                        a.isPledge = true
                        player[currentPlayer].money += a.buyCost / 2

                        for (j in 0 until player[currentPlayer].card.count()){
                            if (player[currentPlayer].card[j].type == "Railway") {
                                val b = player[currentPlayer].card[j] as Railway
                                    --b.railwayCount
                            }
                        }
                    }
                }
                "Utilities" ->{
                    a = player[currentPlayer].card[i] as Utilities
                    if (!a.isPledge && player[currentPlayer].money - rent < 0){
                        a.isPledge = true
                        player[currentPlayer].money += a.buyCost / 2

                        for (j in 0 until player[currentPlayer].card.count()){
                            if (player[currentPlayer].card[j].type == "Utilities") {
                                val b = player[currentPlayer].card[j] as Utilities
                                --b.count
                            }
                        }
                    }
                }
            }

            if (player[currentPlayer].money - rent >= 0)
                return true
        }
        return false
    }

    private fun GameOver(){
        for (i in 0 until card.count()){
            when (card[i].type){
                "Street" -> {
                    val a = card[i] as Street
                    if (a.owner == player[currentPlayer]) {
                        a.owner = null
                        a.houseCount = 0
                        a.isPledge = false
                    }
                }
                "Railway" -> {
                    val a = card[i] as Railway
                    if (a.owner == player[currentPlayer]) {
                        a.owner = null
                        a.isPledge = false
                    }
                }
                "Utilities" -> {
                    val a = card[i] as Utilities
                    if (a.owner == player[currentPlayer]) {
                        a.owner = null
                        a.isPledge = false
                    }
                }
            }
        }
        player.remove(player[currentPlayer])

        --playerCount

        if (player.count() == 1) {
            actionView.text = "Player ${player[0].name} Win!"
            buyButton.visibility = View.GONE
            throwButton.visibility = View.GONE
            passButton.visibility = View.VISIBLE
        }
    }

    private fun FindFullStack(color: String){
        var count = 0
        for (i in 0 until player[currentPlayer].card.count()){
            if (player[currentPlayer].card[i].type == "Street") {
                val a = player[currentPlayer].card[i] as Street
                if (a.color == color)
                    ++count

                if (count == ColorToCount(color)) {
                    for (j in 0 until player[currentPlayer].card.count()) {
                        val b = player[currentPlayer].card[j] as Street
                        if (b.color == color)
                            b.isFullPack = true
                    }
                    return
                }
            }
        }
    }

    private fun ifIsFull(): Int{
        for (i in 0 until player[currentPlayer].card.count()){
            if (player[currentPlayer].card[i] == card[currentField]) {
                val a = player[currentPlayer].card[i] as Street
                return if (a.isFullPack)
                    i
                else
                    -1
            }
        }
        return -1
    }

    private fun ColorToCount(color: String): Int{
        when (color){
            "Dark Blue" -> return 2
            "Brown" -> return 2
            "Blue" -> return 3
            "Pink" -> return 3
            "Orange" -> return 3
            "Red" -> return 3
            "Yellow" -> return 3
            "Green" -> return 3
        }
        return -1
    }

    private fun InitCards(){
        card.add(Parking())
        card.add(Street("Old Kent Road",2, "Brown", mutableListOf(10, 30, 90, 160, 250), 60, 50))
        card.add(Chance())
        card.add(Street("WhiteChapel Road",4, "Brown", mutableListOf(20, 60, 180, 320, 450), 60, 50))
        card.add(Tax())
        card.add(Railway("Kings Cross Station"))
        card.add(Street("The Angel Islington",6, "Blue", mutableListOf(30, 90, 270, 400, 550), 100, 50))
        card.add(Chance())
        card.add(Street("Euston Road",6, "Blue", mutableListOf(30, 90, 270, 400, 550), 100, 50))
        card.add(Street("Pentonville Road",8, "Blue", mutableListOf(40, 100, 300, 450, 600), 120, 50))
        card.add(Parking())
        card.add(Street("Pall Mall",10, "Pink", mutableListOf(50, 150, 450, 625, 750), 140, 100))
        card.add(Utilities("Electric Company"))
        card.add(Street("Whitehall",10, "Pink", mutableListOf(50, 150, 450, 625, 750), 140, 100))
        card.add(Street("Northumrl'd Avenue",12, "Pink", mutableListOf(60, 180, 500, 700, 900), 160, 100))
        card.add(Railway("Marylebone Station"))
        card.add(Street("Bow Street",14, "Orange", mutableListOf(70, 200, 550, 750, 950), 180, 100))
        card.add(Chance())
        card.add(Street("Marlborough Street",14, "Orange", mutableListOf(70, 200, 550, 750, 950), 180, 100))
        card.add(Street("Vine Street",16, "Orange", mutableListOf(80, 220, 600, 800, 1000), 200, 100))
        card.add(Parking())
        card.add(Street("Stand",18, "Red", mutableListOf(90, 250, 700, 875, 1050), 220, 150))
        card.add(Chance())
        card.add(Street("Fleet Street",18, "Red", mutableListOf(90, 250, 700, 875, 1050), 220, 150))
        card.add(Street("Trafalgar Square",20, "Red", mutableListOf(100, 300, 750, 925, 1100), 240, 150))
        card.add(Railway("Fenchurch St. Station"))
        card.add(Street("Leicester Square",22, "Yellow", mutableListOf(110, 330, 800, 975, 1150), 260, 150))
        card.add(Street("Coventry Street",22, "Yellow", mutableListOf(110, 330, 800, 975, 1150), 260, 150))
        card.add(Utilities("Water Works"))
        card.add(Street("Piccadilly",24, "Yellow", mutableListOf(120, 360, 850, 1025, 1200), 280, 150))
        card.add(Prison())
        card.add(Street("Regent Street",26, "Green", mutableListOf(130, 390, 900, 1100, 1275), 300, 200))
        card.add(Street("Oxford Street",26, "Green", mutableListOf(130, 390, 900, 1100, 1275), 300, 200))
        card.add(Chance())
        card.add(Street("Bond Street",28, "Green", mutableListOf(150, 450, 1000, 1200, 1400), 320, 200))
        card.add(Railway("Liverpool St. Station"))
        card.add(Chance())
        card.add(Street("Park Line",35, "Dark Blue", mutableListOf(175, 500, 1100, 1300, 1500), 350, 200))
        card.add(Tax())
        card.add(Street("Mayfair",50, "Dark Blue", mutableListOf(200, 600, 1400, 1700, 2000), 400, 200))
    }

    private fun InitPlayers(count: Int){
        for (i in 0 until count){
            player.add(Player("Player $i"))
        }
    }

    private fun CreatePlayers(){
        for (i in 0 until playerCount){

            when (i) {
                    0 -> {
                        val cs = ConstraintSet()
                        cs.clone(gameField)
                        cs.connect(player0.id, ConstraintSet.RIGHT, gameField.id, ConstraintSet.RIGHT, playerMargin[i].first)
                        cs.connect(player0.id, ConstraintSet.BOTTOM, gameField.id, ConstraintSet.BOTTOM, playerMargin[i].second)
                        cs.applyTo(gameField)
                        player0.visibility = View.VISIBLE
                    }
                1 -> {
                    val cs = ConstraintSet()
                    cs.clone(gameField)
                    cs.connect(player1.id, ConstraintSet.RIGHT, gameField.id, ConstraintSet.RIGHT, playerMargin[i].first)
                    cs.connect(player1.id, ConstraintSet.BOTTOM, gameField.id, ConstraintSet.BOTTOM, playerMargin[i].second)
                    cs.applyTo(gameField)
                    player1.visibility = View.VISIBLE
                }
                2 -> {
                    val cs = ConstraintSet()
                    cs.clone(gameField)
                    cs.connect(player2.id, ConstraintSet.RIGHT, gameField.id, ConstraintSet.RIGHT, playerMargin[i].first)
                    cs.connect(player2.id, ConstraintSet.BOTTOM, gameField.id, ConstraintSet.BOTTOM, playerMargin[i].second)
                    cs.applyTo(gameField)
                    player2.visibility = View.VISIBLE
                }
                3 -> {
                    val cs = ConstraintSet()
                    cs.clone(gameField)
                    cs.connect(player3.id, ConstraintSet.RIGHT, gameField.id, ConstraintSet.RIGHT, playerMargin[i].first)
                    cs.connect(player3.id, ConstraintSet.BOTTOM, gameField.id, ConstraintSet.BOTTOM, playerMargin[i].second)
                    cs.applyTo(gameField)
                    player3.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun ShowMoney(){
        var s = "Money\n"
        for (i in 0 until player.count()){
            s += "Player$i: ${player[i].money}\n"
        }
        playersMoney.text = s
    }

    private fun MovePlayer(step: Int, currentField: Int){
        val cs = gameField.getChildAt(12 + currentPlayer).layoutParams as ConstraintLayout.LayoutParams
        if (currentField >=0 && currentField < 10) {
            val c = step + currentField
            if (c > 10) {

                val top = c % 10
                val left = step - top

                cs.marginEnd += left * 120
                cs.bottomMargin += top * 65
            } else {
                cs.marginEnd += step * 120
            }
        }else if (currentField >=10 && currentField < 20) {
            val c = step + currentField
            if (c > 20) {

                val left = c % 10
                val top = step - left

                cs.marginEnd -= left * 120
                cs.bottomMargin += top * 65
            } else {
                cs.bottomMargin += step * 65
            }
        } else if (currentField >=20 && currentField < 30) {
            val c = step + currentField
            if (c > 30) {

                val top = c % 10
                val left = step - top

                cs.marginEnd -= left * 120
                cs.bottomMargin -= top * 65
            } else {
                cs.marginEnd -= step * 120
            }
        } else {
            val c = step + currentField
            if (c > 40) {

                val left = c % 10
                val top = step - left

                cs.marginEnd += left * 120
                cs.bottomMargin -= top * 65
            } else {
                cs.bottomMargin -= step * 65
            }
        }
        gameField.getChildAt(12 + currentPlayer).layoutParams = cs
    }
}