package com.shokar.monopoly

class Utilities(override val name: String,
                override val type: String = "Utilities",
                override val buyCost: Int = 150,
                var isPledge: Boolean = false,
                var count: Int = 0): Card(name, type, buyCost) {

    var owner: Player? = null
    var firstNum = 0
    var secondNum = 0

    override fun action() {

    }

    override fun rentCost(): Int {
        return if (count == 2)
            (firstNum + secondNum) * 10
        else
            (firstNum + secondNum) * 4
    }

    override fun freeCard() {

    }

    fun setNums(first: Int, second: Int){
        firstNum = first
        secondNum = second
    }
}