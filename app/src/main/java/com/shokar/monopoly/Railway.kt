package com.shokar.monopoly

class Railway(override val name: String,
              override val type: String = "Railway",
              override val buyCost: Int = 200,
              var isPledge: Boolean = false,
              var railwayCount: Int = 0): Card(name, type, buyCost) {

    var owner: Player? = null

    override fun action() {

    }

    override fun rentCost(): Int {
        return when(railwayCount){
            1 -> 50
            2 -> 100
            3 -> 150
            4 -> 200
            else -> -1
        }
    }

    override fun freeCard() {
        railwayCount = 0
    }
}