package com.shokar.monopoly

class Parking(override val name: String = "Parking",
              override val type: String = "Parking"): Card(name, type, 99999) {
    override fun action() {

    }

    override fun rentCost(): Int {
        return 0
    }

    override fun freeCard() {

    }
}