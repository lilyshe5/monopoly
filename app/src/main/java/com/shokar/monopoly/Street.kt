package com.shokar.monopoly

class Street(override val name: String,
             val rent : Int,
             val color: String,
             val houseRent: MutableList<Int>,
             override val buyCost: Int,
             val houseCost: Int,
             var isPledge: Boolean = false,
             val sellCost: Int = buyCost / 2,
             override val type: String = "Street",
             var isFullPack: Boolean = false,
             var houseCount: Int = 0) : Card(name, type, buyCost) {

    var owner: Player? = null

    override fun action() {

    }

    override fun rentCost(): Int {
        if (houseCount == 0){
            return if (isFullPack)
                rent * 2
            else
                rent
        } else
            when (houseCount){
                1 -> return houseRent[0]
                2 -> return houseRent[1]
                3 -> return houseRent[2]
                4 -> return houseRent[3]
                5 -> return houseRent[4]
                else -> {
                    return -1
                }
            }
    }

    override fun freeCard() {
        isFullPack = false
        houseCount = 0
    }

    companion object {
        private const val COLOR_DARK_BLUE = "Dark Blue"
        private const val COLOR_BROWN = "Brown"
        private const val COLOR_BLUE = "Blue"
        private const val COLOR_PINK = "Pink"
        private const val COLOR_ORANGE = "Orange"
        private const val COLOR_RED = "Red"
        private const val COLOR_YELLOW = "Yellow"
        private const val COLOR_GREEN = "Green"
    }
}